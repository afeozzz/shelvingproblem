# This solution is a way far from ideal.

If I had spent more time

    1. I would've written UI tests
    2. I would've checked failure scenarios by unit tests
    3. I would've done proper layering.
    4. I would've separated whether data models or domain models from UI models
    5. I would've played with Compose probably
    6. I would've done different warehouse layouts

I've used https://github.com/airbnb/mavericks for saving my time.

For solving shelving problem I did the following:

    1. We can reflect our warehouse layout by binary matrix
       [1 0 1 1 0 1]
       [1 0 1 1 0 1]
       [1 0 1 1 0 1]
       [1 0 0 0 0 1]
       [1 1 1 1 1 1]

    - 1 means rack
    - 0 means space for going

    2. We put our starting point on any empty space
    3. Run BFS method for going through empty cells
    4. Reach racks on neighbor cells if possible