package io.afeozzz.dija.data

import io.afeozzz.dija.domain.Point
import io.afeozzz.dija.domain.WarehouseModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.util.*

interface WarehouseRepository {
    fun getWarehouseModel(): Flow<WarehouseModel>

    fun exploreRacks(racks: Array<IntArray>, startingPoint: Point, targetRacksCount: Int): Flow<Int>
}

class WarehouseRepositoryImpl(
    private val dispatcher: CoroutineDispatcher
) : WarehouseRepository {

    private data class Node(val point: Point, val distance: Int)

    override fun getWarehouseModel(): Flow<WarehouseModel> = flow {
        delay(5 * 1000)
        emit(
            WarehouseModel(
                matrix = arrayOf(
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 0, 0, 0, 1),
                    intArrayOf(1, 1, 1, 1, 1, 1)
                ),
                formattedMatrix = """
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 1 1 0 1 ] 
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 0 0 0 1 ]
                    [ 1 1 1 1 1 1 ]
                """.trimIndent()
            )
        )
    }.flowOn(dispatcher)

    override fun exploreRacks(
        racks: Array<IntArray>,
        startingPoint: Point,
        targetRacksCount: Int
    ): Flow<Int> = flow {

        fun isValid(array: Array<IntArray>, x: Int, y: Int, value: Int) =
            !(x < 0 || x >= array.size || y < 0 || y >= array.size) && array[x][y] == value

        if (!isValid(racks, startingPoint.x, startingPoint.y, 0)) {
            emit(-1)
            return@flow
        }

        val rows = intArrayOf(-1, 0, 0, 1)
        val columns = intArrayOf(0, -1, 1, 0)

        val queue = LinkedList<Node>().apply { offer(Node(startingPoint, 0)) }
        val reachedRacks = hashSetOf<Point>()
        val visitedShelves = Array(racks.size + 1) { BooleanArray(racks[0].size + 1) }
        visitedShelves[startingPoint.x][startingPoint.y] = true
        while (!queue.isEmpty()) {
            val node = queue.poll()
            val currentPoint = node.point

            if (reachedRacks.size >= targetRacksCount) {
                emit(node.distance)
                return@flow
            }

            for (i in rows.indices) {
                val x = currentPoint.x + rows[i]
                val y = currentPoint.y + columns[i]

                if (isValid(racks, x, y, 1) && !reachedRacks.contains(Point(x, y))) {
                    reachedRacks.add(Point(x, y))
                }

                if (isValid(racks, x, y, 0) && !visitedShelves[x][y]) {
                    visitedShelves[x][y] = true
                    queue.add(Node(Point(x, y), node.distance + 1))
                }
            }

        }
        emit(-1)
    }.flowOn(dispatcher)

}