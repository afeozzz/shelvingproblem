package io.afeozzz.dija

import android.app.Application
import com.airbnb.mvrx.Mavericks
import io.afeozzz.dija.data.WarehouseRepositoryImpl
import kotlinx.coroutines.Dispatchers

class DijaApplication : Application() {

    val warehouseRepository = WarehouseRepositoryImpl(Dispatchers.IO)

    override fun onCreate() {
        super.onCreate()
        Mavericks.initialize(this)
    }
}