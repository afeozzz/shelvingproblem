package io.afeozzz.dija.ui.warehouse

import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import io.afeozzz.dija.DijaApplication
import io.afeozzz.dija.data.WarehouseRepository

class WarehouseViewModel(
    state: WarehouseScreenState,
    private val warehouseRepository: WarehouseRepository,
) : MavericksViewModel<WarehouseScreenState>(state) {

    fun loadWarehouseModel() {
        setState { copy(shortestPathDistance = null) }
        warehouseRepository.getWarehouseModel().execute { copy(warehouseModel = it) }
    }

    fun handleStartingPointInput(startingInputPoint: String) {
        setState { copy(startingInputPoint = startingInputPoint) }
    }

    fun handleTargetRacks(inputTargetRacks: String) {
        setState { copy(inputTargetRacks = inputTargetRacks) }
    }

    fun exploreRacks() = withState { state ->
        val model = state.warehouseModel() ?: error("Matrix is not presented")
        warehouseRepository.exploreRacks(model.matrix, state.pointsFromInput, state.targetRacksAsInt)
            .execute { copy(shortestPathDistance = it()) }
    }

    companion object : MavericksViewModelFactory<WarehouseViewModel, WarehouseScreenState> {
        override fun create(
            viewModelContext: ViewModelContext,
            state: WarehouseScreenState
        ): WarehouseViewModel {
            val warehouseRepository =
                viewModelContext.app<DijaApplication>().warehouseRepository

            return WarehouseViewModel(state, warehouseRepository)
        }
    }
}