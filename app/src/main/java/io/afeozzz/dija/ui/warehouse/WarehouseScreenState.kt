package io.afeozzz.dija.ui.warehouse

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.PersistState
import com.airbnb.mvrx.Uninitialized
import io.afeozzz.dija.domain.Point
import io.afeozzz.dija.domain.WarehouseModel

data class WarehouseScreenState(
    val warehouseModel: Async<WarehouseModel> = Uninitialized,
    @PersistState val shortestPathDistance: Int? = null,
    @PersistState val startingInputPoint: String = "",
    @PersistState val inputTargetRacks: String = ""
) : MavericksState {
    val pointsFromInput by lazy {
        startingInputPoint.split(", ").let { Point(it[0].toInt(), it[1].toInt()) }
    }
    val targetRacksAsInt by lazy { inputTargetRacks.toInt() }

    val infoText = shortestPathDistance?.let { distance ->
        if (distance != -1) {
            "Shortest path is $distance"
        } else {
            "Shortest path is not reachable or you put a starting point onto a rack"
        }
    }
}