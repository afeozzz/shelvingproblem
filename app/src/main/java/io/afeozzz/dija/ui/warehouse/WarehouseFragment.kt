package io.afeozzz.dija.ui.warehouse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import com.airbnb.mvrx.*
import io.afeozzz.dija.R
import kotlinx.android.synthetic.main.fragment_warehouse.*

class WarehouseFragment : Fragment(), MavericksView {

    private val viewModel: WarehouseViewModel by activityViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_warehouse, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        warehouseModelTextView.setOnClickListener { viewModel.loadWarehouseModel() }
        exploreButton.setOnClickListener { viewModel.exploreRacks() }
        racksCountEditText.addTextChangedListener { text -> viewModel.handleTargetRacks(text.toString()) }
        startingPointEditText.addTextChangedListener { text ->
            viewModel.handleStartingPointInput(
                text.toString()
            )
        }
    }

    override fun invalidate() = withState(viewModel) { state ->
        val model = state.warehouseModel
        warehouseModelTextView.isVisible = model is Uninitialized || model is Success
        progressBar.isVisible = model is Loading
        exploreButton.isEnabled = model is Success
        when (model) {
            Uninitialized -> warehouseModelTextView.text = "Tap to load warehouse model"
            is Success -> warehouseModelTextView.text = model().formattedMatrix
            else -> Unit
        }

        collectDistanceInformation(state)
    }

    private fun collectDistanceInformation(state: WarehouseScreenState) {
        infoTextView.isVisible = state.shortestPathDistance != null
        infoTextView.text = state.infoText
    }
}