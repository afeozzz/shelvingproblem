package io.afeozzz.dija.domain

import java.io.Serializable


data class WarehouseModel(
    val matrix: Array<IntArray>,
    val formattedMatrix: String,
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WarehouseModel

        if (!matrix.contentDeepEquals(other.matrix)) return false
        if (formattedMatrix != other.formattedMatrix) return false

        return true
    }

    override fun hashCode(): Int {
        var result = matrix.contentDeepHashCode()
        result = 31 * result + formattedMatrix.hashCode()
        return result
    }
}