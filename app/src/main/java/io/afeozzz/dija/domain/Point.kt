package io.afeozzz.dija.domain

data class Point(val x: Int, val y: Int)