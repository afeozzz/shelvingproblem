package io.afeozzz.dija

import app.cash.turbine.test
import io.afeozzz.dija.data.WarehouseRepositoryImpl
import io.afeozzz.dija.domain.Point
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

@ExperimentalTime
@ExperimentalCoroutinesApi
class WarehouseRepositoryImplTest {

    private val dispatcher = TestCoroutineDispatcher()
    private val repository = WarehouseRepositoryImpl(dispatcher)
    private val matrixStub = arrayOf(
        intArrayOf(1, 0, 1, 1, 0, 1),
        intArrayOf(1, 0, 1, 1, 0, 1),
        intArrayOf(1, 0, 1, 1, 0, 1),
        intArrayOf(1, 0, 0, 0, 0, 1),
        intArrayOf(1, 1, 1, 1, 1, 1)
    )

    @Test
    fun `repo emit data after 5 seconds`() = dispatcher.runBlockingTest {
        repository.getWarehouseModel().test(5.seconds) {
            assertEquals(
                expectItem().formattedMatrix,
                """
                [ 1 0 1 1 0 1 ]
                [ 1 0 1 1 0 1 ] 
                [ 1 0 1 1 0 1 ]
                [ 1 0 0 0 0 1 ]
                [ 1 1 1 1 1 1 ]
            """.trimIndent()
            )
            expectComplete()
        }
    }

    @Test
    fun `explore should emit NOT DEFINED when starting point on to rack`() =
        dispatcher.runBlockingTest {
            val flow = repository.exploreRacks(matrixStub, Point(0, 0), 10).test {
                assertEquals(expectItem(), -1)
                expectComplete()
            }
        }

    @Test
    fun `explore should emit NOT DEFINED when target racks is not reachable`() =
        dispatcher.runBlockingTest {
            repository.exploreRacks(matrixStub, Point(0, 1), 329329).test {
                assertEquals(expectItem(), -1)
                expectComplete()
            }
        }

    @Test
    fun `explore should emit shortest path 1 when target racks 1`() = dispatcher.runBlockingTest {
        repository.exploreRacks(matrixStub, Point(0, 1), 1).test {
            assertEquals(expectItem(), 1)
            expectComplete()
        }
    }

    @Test
    fun `explore should emit shortest path 6 when target racks 10`() = dispatcher.runBlockingTest {
        repository.exploreRacks(matrixStub, Point(0, 1), 10).test {
            assertEquals(expectItem(), 6)
            expectComplete()
        }
    }


}