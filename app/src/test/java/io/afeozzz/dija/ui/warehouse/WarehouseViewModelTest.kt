package io.afeozzz.dija.ui.warehouse

import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.test.MvRxTestRule
import com.airbnb.mvrx.withState
import com.nhaarman.mockito_kotlin.*
import io.afeozzz.dija.data.WarehouseRepository
import io.afeozzz.dija.domain.Point
import io.afeozzz.dija.domain.WarehouseModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.ClassRule
import org.junit.Test

class WarehouseViewModelTest {

    private val repository = mock<WarehouseRepository>()

    @Test
    fun loadWarehouseModel_success() {
        val warehouseModel = MutableSharedFlow<WarehouseModel>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

        whenever(repository.getWarehouseModel()).thenReturn(warehouseModel)
        val viewModel = WarehouseViewModel(WarehouseScreenState(), repository)
        viewModel.loadWarehouseModel()
        verify(repository).getWarehouseModel()
        withState(viewModel) {
            assertEquals(null, it.shortestPathDistance)
            assertTrue(it.warehouseModel is Loading)
        }
        warehouseModel.tryEmit(
            WarehouseModel(
                matrix = arrayOf(
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 1, 1, 0, 1),
                    intArrayOf(1, 0, 0, 0, 0, 1),
                    intArrayOf(1, 1, 1, 1, 1, 1)
                ),
                formattedMatrix = """
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 1 1 0 1 ] 
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 0 0 0 1 ]
                    [ 1 1 1 1 1 1 ]
                """.trimIndent()
            )
        )
        withState(viewModel) { assertTrue(it.warehouseModel is Success) }
    }

    @Test
    fun handleStartingPointInput_success() {
        val viewModel = WarehouseViewModel(WarehouseScreenState(), repository)
        viewModel.handleStartingPointInput("0, 1")
        withState(viewModel) {
            assertEquals(it.startingInputPoint, "0, 1")
            assertEquals(it.pointsFromInput, Point(0, 1))
        }
    }

    @Test
    fun handleTargetRacks_success() {
        val viewModel = WarehouseViewModel(WarehouseScreenState(), repository)
        viewModel.handleTargetRacks("5")
        withState(viewModel) {
            assertEquals(it.inputTargetRacks, "5")
            assertEquals(it.targetRacksAsInt, 5)
        }
    }

    @Test
    fun exploreRacks_success() {
        val viewModel = WarehouseViewModel(WarehouseScreenState(), repository)
        whenever(repository.getWarehouseModel()).thenReturn(flow {
            emit(
                WarehouseModel(
                    matrix = arrayOf(
                        intArrayOf(1, 0, 1, 1, 0, 1),
                        intArrayOf(1, 0, 1, 1, 0, 1),
                        intArrayOf(1, 0, 1, 1, 0, 1),
                        intArrayOf(1, 0, 0, 0, 0, 1),
                        intArrayOf(1, 1, 1, 1, 1, 1)
                    ),
                    formattedMatrix = """
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 1 1 0 1 ] 
                    [ 1 0 1 1 0 1 ]
                    [ 1 0 0 0 0 1 ]
                    [ 1 1 1 1 1 1 ]
                """.trimIndent()
                )
            )
        })
        viewModel.loadWarehouseModel()
        viewModel.handleTargetRacks("10")
        viewModel.handleStartingPointInput("0, 1")
        whenever(repository.exploreRacks(any(), eq(Point(0, 1)), eq(10))).thenReturn(flow {
            emit(42)
        })
        viewModel.exploreRacks()
        withState(viewModel) {
            assertEquals(it.shortestPathDistance, 42)
        }
    }


    companion object {
        @JvmField
        @ClassRule
        val mvrxTestRule = MvRxTestRule()
    }
}